# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Workspace::Application.config.secret_key_base = 'f2559c1aee989596ca1aaa0fcef38aef952479a071d15389510f35a302f31029bfed4070f555616069f319a892e1c59daedba0d90ba732b56ddb6518909e33cd'
